const express = require('express');
const app = express();
const port = 8000;
const fs = require('fs');
let userId = require('./id-pass.json');
let dataRev = require('./data-review.json');

app.set('view engine', 'ejs');

app.use(express.static('node_modules'));
app.use(express.static(__dirname + '/views'));
app.use(express.json());

app.get('/', (request, response) => {
  response.render('ch3', {
    name: userId[0].name,
    data: dataRev,
  });
});

app.get('/game', (request, response) => {
  response.render('ch4', {
    title: 'ROCK PAPER SCISSOR',
    displayId: true,
    viewOnly: false,
  });
});

app.post('/login', (request, response) => {
  const { username, password } = request.body;
  const user = userId.find((user) => user.username === username);

  if (user) {
    if (user.password === password) {
      response.status(200).json({ status: 'success', ...user });
    } else {
      response.status(403).json({ status: 'wrong-password' });
    }
  } else {
    response.status(404).json({ status: 'no-username' });
  }
});

app.listen(port, () => {
  console.log('Server constructed!');
});
